Blockly.Blocks['welcome'] = {
  init: function() {
    this.appendValueInput("LASTNAME")
        .setCheck("String")
        .appendField("姓");
    this.appendValueInput("FIRSTNAME")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("名");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(270);
    this.setTooltip('Print welcome message');
    this.setHelpUrl('http://www.google.com');
  }
};

Blockly.JavaScript['welcome'] = function(block) {
  var value_lastname = Blockly.JavaScript.valueToCode(block, 'LASTNAME', Blockly.JavaScript.ORDER_ATOMIC);
  var value_firstname = Blockly.JavaScript.valueToCode(block, 'FIRSTNAME', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'window.alert("Welcome '+value_lastname + value_firstname +'");\n';
  return code;
};


Blockly.Python['welcome'] = function(block) {
  var value_lastname = Blockly.Python.valueToCode(block, 'LASTNAME', Blockly.Python.ORDER_ATOMIC);
  var value_firstname = Blockly.Python.valueToCode(block, 'FIRSTNAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = 'print("Welcome '+value_lastname + value_firstname +'")\n';
  return code;
};




