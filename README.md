1. 教材
https://yinhang2.gitbooks.io/-blockly/content/

2. 在线使用
官网（墙） https://developers.google.com/blockly/
Blockly games（墙） https://blockly-games.appspot.com/

3. 本地使用
Github Blockly: https://github.com/google/blockly
Blockly games: https://github.com/google/blockly-games
Closure-library: https://github.com/google/closure-library

4. Git
GitHub:  http://github.com
GitLab:   http://gitlab.com
GitBook: http://gitbooks.com
COOC:    https://cooc-china.github.io/
